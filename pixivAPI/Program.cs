﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pixivAPI {
    static class Program {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main() {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            while(LoginForm.login() != DialogResult.OK) { }
            Application.Run(new Form1());
        }

        //TODO fix image centering
        //TODO increase download speed
        //TODO filter by xusers
        //TODO asynchronous thumbnail downloads
    }
}
