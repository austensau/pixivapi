﻿using Pixeez;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pixivAPI {
    public partial class Form1 : Form {
        private List<Pixeez.Objects.Work> searchResults;
        private DataGridViewImageCell selectedCell;
        bool gridPrepped = false;

        public Form1() {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e) {
            
        }

        /// <summary>
        /// Searches, sorts, then downloads thumbnails based off searchBox text.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void startButton_Click(object sender, EventArgs e) {
            if(!gridPrepped) {
                prepareGrid();
                gridPrepped = true;
            }

            textBox1.Text += "Start: " + DateTime.Now.Second;
            searchResults = Task.Run(async () => await ApiHandler.getWorks(searchBox.Text)).Result; //get search results
            var results = sortResults(); //sort results
            textBox1.Text += " Finish: " + DateTime.Now.Second + "\r\n";

            Task.Run(async () => await downloadThumbnails(results));

        }

        private Task downloadThumbnails(IOrderedEnumerable<Pixeez.Objects.Work> results) {
            for(int i = 0; i < dataGridView1.RowCount; i++) { //download works
                for(int j = 0; j < dataGridView1.ColumnCount; j++) {
                    if(searchResults.Count <= (i * dataGridView1.ColumnCount) + j) {
                        break;
                    }
                    Pixeez.Objects.Work work = results.ToArray()[(i * dataGridView1.ColumnCount) + j];
                    //Pixeez.Objects.Work work = searchResults[(i * dataGridView1.ColumnCount) + j];
                    string uri = work.ImageUrls.Small;

                    dataGridView1.Rows[i].Height = dataGridView1.Height / 4 - 1; //set row height
                    var image = Task.Run(async () => await ApiHandler.downloadWork(uri)).GetAwaiter();
                    using(var ms = new MemoryStream(image.GetResult())) {
                        dataGridView1.Rows[i].Cells[j].Value = new Bitmap(ms);
                        dataGridView1.Rows[i].Cells[j].Tag = work.Title;
                    }
                }
            }
            return null;
        }

        private IOrderedEnumerable<Pixeez.Objects.Work> sortResults() {
            string sortMethod = (string)comboBox1.SelectedItem;
            if(sortMethod == "Sort by Score") {
                return searchResults.OrderByDescending(x => x.Stats?.Score);
            } else if (sortMethod == "Sort by Views") {
                return searchResults.OrderByDescending(x => x.Stats?.ViewsCount);
            } else if(sortMethod == "Sort by Favorites") {
                return searchResults.OrderByDescending(x => x.Stats?.FavoritedCount?.Public + x.Stats?.FavoritedCount?.Private);
            } else {
                textBox1.Text += "Invalid Item: " + sortMethod + "\r\n";
                throw new ArgumentException();
            }
        }

        private void prepareGrid() {
            dataGridView1.RowCount = 4; //setup image grid
            for(int i = 0; i < 5; i++) {
                dataGridView1.Columns.Insert(i, new DataGridViewImageColumn());
                dataGridView1.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
            dataGridView1.Columns.RemoveAt(5);
            dataGridView1.ClearSelection();
        }

        private void saveButton_Click(object sender, EventArgs e) {
            if(selectedCell.Value == null) {
                textBox1.Text += "No cell\n\r";
                return;
            }
            string path = @"E:\Austen\Documents\h\new\app\";
            string title = (string)selectedCell.Tag;
            foreach(char c in Path.GetInvalidFileNameChars()) {
                title = title.Replace(c, '_');
            }
            path += title + ".png";

            textBox1.Text = "\nsaved at: " + path + " \n\r";
            try {
                new Bitmap(pictureBox1.Image).Save(path, ImageFormat.Png);
            } catch(ExternalException) {
                textBox1.Text += "\n\rFailed to save - " + title;
            } catch {
                textBox1.Text = "\n\rFailed to save - stream";
            }
        }


        /// <summary>
        /// When a cell is clicked, the largest version of the containing image is downloaded, and displayed fullscreen.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e) {
            selectedCell = (DataGridViewImageCell)dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex];
            foreach(Pixeez.Objects.Work w in searchResults) {
                if(w.Title == (string)selectedCell.Tag) {
                    var image = Task.Run(async () => await ApiHandler.downloadWork(w.ImageUrls.Large)).GetAwaiter();
                    using(var ms = new MemoryStream(image.GetResult())) {
                        pictureBox1.Image = new Bitmap(ms);
                    }
                }
            }
            pictureBox1.Visible = true;
        }

        private void textBox1_TextChanged_1(object sender, EventArgs e) {

        }

        private void pictureBox1_Click(object sender, EventArgs e) {
            pictureBox1.Visible = false;
        }

        private void Form1_Load(object sender, EventArgs e) {

        }

        private void dataGridView1_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e) {
            dataGridView1.ClearSelection();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e) {

        }
    }
}
