﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pixivAPI {
   public class LoginForm : Form {
        private TextBox usernameBox;
        private TextBox passwordBox;

        public LoginForm() {
            Width = 350;
            Height = 130;
            Text = "Login to Pixiv.com";
            FormBorderStyle = FormBorderStyle.FixedSingle;
            MaximizeBox = false;
            MinimizeBox = false;
            StartPosition = FormStartPosition.CenterScreen;

            usernameBox = new TextBox() { Left = 10, Top = 5 + 23, Width = 150 };
            passwordBox = new TextBox() { Left = 20 + usernameBox.Width, Top = 5 + 23, Width = 150 };
            passwordBox.PasswordChar = '*';
            Label usernameLabel = new Label() { Left = usernameBox.Left, Top = 5, Text = "Username" };
            Label passwordLabel = new Label() { Left = passwordBox.Left, Top = 5, Text = "Password" };
            Button confirmation = new Button() { Text = "Login", Left = (Width / 2) - 60, Width = 100, Top = Height / 2 - 5 };

            Controls.Add(confirmation);
            Controls.Add(usernameLabel);
            Controls.Add(passwordLabel);
            Controls.Add(usernameBox);
            Controls.Add(passwordBox);
            confirmation.Click += Confirmation_Click;
        }

        private void Confirmation_Click(object sender, EventArgs e) {
            DialogResult = DialogResult.OK;
            Close();
        }

        public static DialogResult login() {
            using(LoginForm form = new LoginForm()) {
                try {
                    form.ShowDialog();
                    ApiHandler.login(form.usernameBox.Text, form.passwordBox.Text);
                    return DialogResult.Yes;
                } catch(InvalidOperationException) {
                    MessageBox.Show("Invalid username or password.");
                    return DialogResult.No;
                }
            }
        }
    }
}
