﻿using Pixeez;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Authentication;

namespace pixivAPI {
    class ApiHandler {
        private static Tokens tokens;
        private static string def = "忍野忍";

        /// <summary>
        /// Throws InvalidOperationException if it fails
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        public static async void login(string username, string password) {
            tokens = await Auth.AuthorizeAsync(username, password);
        }

        public static async Task<List<Pixeez.Objects.Work>> getWorks(string query = "忍野忍", int numWorks = 20, bool showr18 = false, bool onlyr18 = false) {
            // Create Tokens
            if(tokens == null) {
                throw new AuthenticationException();
            }
            if(query == "") {
                query = def;
            }

            int pagesRemaining = 1;// numWorks / 20, imageCount = int.MaxValue;
            int PerPage = numWorks;
            int startPage = 1;
            List<Pixeez.Objects.Work> results = new List<Pixeez.Objects.Work>();
            while(pagesRemaining > 0) {
                var search = await tokens.SearchWorksAsync(query, page: startPage, perPage: PerPage, mode: "tag");
                foreach(var s in search) {
                    if(showr18 == false && s.AgeLimit == "r18") { //option to skip explicit
                        continue;
                    } else if(onlyr18 && s.AgeLimit != "r18") { //option to show only explicit
                        continue;
                    }
                    results.Add(s);
                }
                pagesRemaining--;
                startPage++;
            }

            return results;
        }

        public static async Task<Byte[]> downloadWork(string uri) {
            return await tokens.SendRequestAsync(MethodType.GET, uri, null).Result.GetResponseByteArrayAsync();
        }
    }
}
